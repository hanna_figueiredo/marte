package com.hannaf.marte.exploration;

import static java.util.stream.Collectors.toList;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RequestMapping("/explorations")
@RestController
public class ExplorationController {

    @Autowired
    private ExplorationRepository explorationRepository;

    @Autowired
    private ExplorationToExplorationDTOConverter converter;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public ExplorationDTO save(@RequestBody @Valid ExplorationCreateDTO explorationCreateDTO) {
        Exploration exploration = new Exploration(new Coordinate(explorationCreateDTO.getMaxXCoordinate(), explorationCreateDTO.getMaxYCoordinate()));

        return converter.convert(explorationRepository.save(exploration));
    }

    @GetMapping
    public List<ExplorationDTO> list() {
        List<ExplorationDTO> collect = explorationRepository.findAll().stream().map(exploration -> converter.convert(exploration)).collect(toList());

        return collect;
    }

    @GetMapping("/{explorationKey}")
    public ResponseEntity<ExplorationDTO> get(@PathVariable Long explorationKey) {
        Optional<Exploration> exploration = explorationRepository.findById(explorationKey);

        if (exploration.isPresent()) {
            return ResponseEntity.ok(converter.convert(exploration.get()));
        } else {
            return ResponseEntity.notFound().build();
        }
    }

}
