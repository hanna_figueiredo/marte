package com.hannaf.marte.exploration;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.persistence.*;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import com.hannaf.marte.exploration.probe.Probe;

@Entity
public class Exploration {

    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Valid
    @NotNull
    @Embedded
    private Coordinate maxCoordinateToExplore;

    @Valid
    @OneToMany(mappedBy = "exploration")
    private List<Probe> probes = new ArrayList<>();

    @Deprecated
    Exploration() {}

    public Exploration(Coordinate maxCoordinateToExplore) {
        this.maxCoordinateToExplore = maxCoordinateToExplore;
    }

    public Long getId() {
        return id;
    }

    public Coordinate getMaxCoordinateToExplore() {
        return maxCoordinateToExplore;
    }

    public List<Probe> getProbes() {
        return Collections.unmodifiableList(probes);
    }

    public void add(Probe probe) {
        if (probe.getPosition().isBeyondLimit(maxCoordinateToExplore)) {
            throw new IllegalArgumentException(String.format("Probe %s initial value cannot be superior to %s for exploration %s",
                    id, maxCoordinateToExplore, id));
        }

        probes.add(probe);
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Exploration{");
        sb.append("id=").append(id);
        sb.append(", maxCoordinateToExplore=").append(maxCoordinateToExplore);
        sb.append(", probes=").append(probes);
        sb.append('}');
        return sb.toString();
    }

}
