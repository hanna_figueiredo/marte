package com.hannaf.marte.exploration;

import javax.persistence.Embeddable;
import javax.validation.constraints.Min;

import com.hannaf.marte.exploration.probe.Direction;

@Embeddable
public class Coordinate {

    @Min(0)
    private int x;
    @Min(0)
    private int y;

    @Deprecated
    Coordinate() {}

    public Coordinate(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public Coordinate moveBy(Direction direction) {
        if (direction == Direction.N) {
            return new Coordinate(x, y + 1);
        } else if (direction == Direction.S) {
            return new Coordinate(x, y - 1);
        } else if (direction == Direction.E) {
            return new Coordinate(x + 1, y);
        } else {
            return new Coordinate(x - 1, y);
        }
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Coordinate{");
        sb.append("x=").append(x);
        sb.append(", y=").append(y);
        sb.append('}');
        return sb.toString();
    }

}
