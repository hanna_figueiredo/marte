package com.hannaf.marte.exploration.probe;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

public class ProbeCreateDTO {

    @Min(0)
    private int initialXCoordinate;
    @Min(0)
    private int initialYCoordinate;
    @NotNull
    private Direction initialDirection;

    public int getInitialXCoordinate() {
        return initialXCoordinate;
    }

    public ProbeCreateDTO setInitialXCoordinate(int initialXCoordinate) {
        this.initialXCoordinate = initialXCoordinate;
        return this;
    }

    public int getInitialYCoordinate() {
        return initialYCoordinate;
    }

    public ProbeCreateDTO setInitialYCoordinate(int initialYCoordinate) {
        this.initialYCoordinate = initialYCoordinate;
        return this;
    }

    public Direction getInitialDirection() {
        return initialDirection;
    }

    public ProbeCreateDTO setInitialDirection(Direction initialDirection) {
        this.initialDirection = initialDirection;
        return this;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("ProbeCreateDTO{");
        sb.append("initialXCoordinate=").append(initialXCoordinate);
        sb.append(", initialYCoordinate=").append(initialYCoordinate);
        sb.append(", initialDirection=").append(initialDirection);
        sb.append('}');
        return sb.toString();
    }
}
