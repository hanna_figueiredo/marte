package com.hannaf.marte.exploration.probe;

import java.util.ArrayList;
import java.util.List;

import com.hannaf.marte.exploration.probe.command.ExecutedCommandDTO;

public class ProbeDTO {

    private Long id;
    private int coordinateX;
    private int coordinateY;
    private Direction direction;
    private List<ExecutedCommandDTO> executedCommands = new ArrayList<>();

    public Long getId() {
        return id;
    }

    public ProbeDTO setId(Long id) {
        this.id = id;
        return this;
    }

    public int getCoordinateX() {
        return coordinateX;
    }

    public ProbeDTO setCoordinateX(int coordinateX) {
        this.coordinateX = coordinateX;
        return this;
    }

    public int getCoordinateY() {
        return coordinateY;
    }

    public ProbeDTO setCoordinateY(int coordinateY) {
        this.coordinateY = coordinateY;
        return this;
    }

    public Direction getDirection() {
        return direction;
    }

    public ProbeDTO setDirection(Direction direction) {
        this.direction = direction;
        return this;
    }

    public List<ExecutedCommandDTO> getExecutedCommands() {
        return executedCommands;
    }

    public ProbeDTO setExecutedCommands(List<ExecutedCommandDTO> executedCommands) {
        this.executedCommands = executedCommands;
        return this;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("ProbeDTO{");
        sb.append("id=").append(id);
        sb.append(", coordinateX=").append(coordinateX);
        sb.append(", coordinateY=").append(coordinateY);
        sb.append(", direction=").append(direction);
        sb.append(", executedCommands=").append(executedCommands);
        sb.append('}');
        return sb.toString();
    }
}
