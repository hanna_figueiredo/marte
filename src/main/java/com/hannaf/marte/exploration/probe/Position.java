package com.hannaf.marte.exploration.probe;

import javax.persistence.Embeddable;
import javax.persistence.Embedded;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import com.hannaf.marte.exploration.Coordinate;

@Embeddable
public class Position {

    @Valid
    @NotNull
    @Embedded
    private Coordinate coordinate;

    @NotNull
    @Enumerated(EnumType.STRING)
    private Direction direction;

    @Deprecated
    Position() {}

    private Position(Coordinate coordinate, Direction direction) {
        this.coordinate = coordinate;
        this.direction = direction;
    }

    public static Position createInitialBy(Coordinate coordinate, Direction direction) {
        return new Position(coordinate, direction);
    }

    public static Position turnLeftFrom(Position position) {
        return new Position(position.getCoordinate(), position.getDirection().getLeft());
    }

    public static Position turnRightFrom(Position position) {
        return new Position(position.getCoordinate(), position.getDirection().getRight());
    }

    public static Position moveForwardFrom(Position position) {
        Direction direction = position.getDirection();
        Coordinate newCoordinate = position.getCoordinate().moveBy(direction);

        return new Position(newCoordinate, direction);
    }

    public Coordinate getCoordinate() {
        return coordinate;
    }

    public Direction getDirection() {
        return direction;
    }

    public boolean isBeyondLimit(Coordinate maxCoordinateToExplore) {
        return coordinate.getX() > maxCoordinateToExplore.getX() ||
                coordinate.getX() < 0 ||
                coordinate.getY() > maxCoordinateToExplore.getY() ||
                coordinate.getY() < 0;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Position{");
        sb.append("coordinate=").append(coordinate);
        sb.append(", direction=").append(direction);
        sb.append('}');
        return sb.toString();
    }
}
