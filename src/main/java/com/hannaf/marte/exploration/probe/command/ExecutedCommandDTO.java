package com.hannaf.marte.exploration.probe.command;

import java.time.LocalDateTime;

public class ExecutedCommandDTO {

    private Command command;
    private LocalDateTime createdAt;

    public Command getCommand() {
        return command;
    }

    public ExecutedCommandDTO setCommand(Command command) {
        this.command = command;
        return this;
    }

    public LocalDateTime getCreatedAt() {
        return createdAt;
    }

    public ExecutedCommandDTO setCreatedAt(LocalDateTime createdAt) {
        this.createdAt = createdAt;
        return this;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("ExecutedCommandDTO{");
        sb.append("command=").append(command);
        sb.append(", createdAt=").append(createdAt);
        sb.append('}');
        return sb.toString();
    }
}
