package com.hannaf.marte.exploration.probe;

public enum Direction {

    N {
        @Override
        public Direction getLeft() {
            return Direction.W;
        }

        @Override
        public Direction getRight() {
            return Direction.E;
        }
    },
    E {
        @Override
        public Direction getLeft() {
            return Direction.N;
        }

        @Override
        public Direction getRight() {
            return Direction.S;
        }
    },
    S {
        @Override
        public Direction getLeft() {
            return Direction.E;
        }

        @Override
        public Direction getRight() {
            return Direction.W;
        }
    },
    W {
        @Override
        public Direction getLeft() {
            return Direction.S;
        }

        @Override
        public Direction getRight() {
            return Direction.N;
        }
    };

    public abstract Direction getLeft();

    public abstract Direction getRight();
}
