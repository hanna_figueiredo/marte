package com.hannaf.marte.exploration.probe;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

public interface ProbeRepository extends CrudRepository<Probe, Long> {

    Optional<Probe> findById(Long probeId);
}
