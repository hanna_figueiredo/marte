package com.hannaf.marte.exploration.probe.command;

import static com.hannaf.marte.exploration.probe.Position.moveForwardFrom;
import static com.hannaf.marte.exploration.probe.Position.turnLeftFrom;
import static com.hannaf.marte.exploration.probe.Position.turnRightFrom;

import com.hannaf.marte.exploration.probe.Position;

public enum Command {

    LEFT {
        @Override
        public Position getNewPositionBasedOnActual(Position position) {
            return turnLeftFrom(position);
        }
    },
    RIGHT {
        @Override
        public Position getNewPositionBasedOnActual(Position position) {
            return turnRightFrom(position);
        }
    },
    MOVE {
        @Override
        public Position getNewPositionBasedOnActual(Position position) {
            return moveForwardFrom(position);
        }
    };

    public abstract Position getNewPositionBasedOnActual(Position position);
}
