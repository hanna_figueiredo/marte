package com.hannaf.marte.exploration.probe.command;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class ExecutedCommandToExecutedCommandDTOConverter implements Converter<ExecutedCommand, ExecutedCommandDTO> {

    @Override
    public ExecutedCommandDTO convert(ExecutedCommand executedCommand) {
        ExecutedCommandDTO executedCommandDTO = new ExecutedCommandDTO();

        executedCommandDTO.setCommand(executedCommand.getCommand());
        executedCommandDTO.setCreatedAt(executedCommand.getCreatedAt());

        return executedCommandDTO;
    }
}
