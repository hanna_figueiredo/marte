package com.hannaf.marte.exploration.probe;

import static java.util.stream.Collectors.toList;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.hannaf.marte.exploration.probe.command.ExecutedCommandDTO;
import com.hannaf.marte.exploration.probe.command.ExecutedCommandToExecutedCommandDTOConverter;

@Component
public class ProbeToProbeDTOConverter implements Converter<Probe, ProbeDTO> {

    @Autowired
    private ExecutedCommandToExecutedCommandDTOConverter converter;

    @Override
    public ProbeDTO convert(Probe probe) {
        ProbeDTO probeDTO = new ProbeDTO();

        probeDTO.setId(probe.getId());
        probeDTO.setCoordinateX(probe.getPosition().getCoordinate().getX());
        probeDTO.setCoordinateY(probe.getPosition().getCoordinate().getY());
        probeDTO.setDirection(probe.getPosition().getDirection());

        List<ExecutedCommandDTO> commands = probe.getExecutedCommands().stream().map(command -> converter.convert(command)).collect(toList());

        probeDTO.setExecutedCommands(commands);


        return probeDTO;
    }
}
