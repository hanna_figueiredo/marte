package com.hannaf.marte.exploration.probe.command;

import java.util.ArrayList;
import java.util.List;

import javax.validation.constraints.Size;

public class CommandsDTO {

    @Size(min = 1)
    private List<Command> commands = new ArrayList<>();

    public List<Command> getCommands() {
        return commands;
    }

    public CommandsDTO setCommands(List<Command> commands) {
        this.commands = commands;
        return this;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("CommandsDTO{");
        sb.append("commands=").append(commands);
        sb.append('}');
        return sb.toString();
    }
}
