package com.hannaf.marte.exploration.probe.command;

import java.time.LocalDateTime;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
public class ExecutedCommand {

    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Enumerated(EnumType.STRING)
    private Command command;

    private LocalDateTime createdAt = LocalDateTime.now();

    @Deprecated
    ExecutedCommand() {}

    public ExecutedCommand(Command command) {
        this.command = command;
    }

    public Command getCommand() {
        return command;
    }

    public LocalDateTime getCreatedAt() {
        return createdAt;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("ExecutedCommand{");
        sb.append("id=").append(id);
        sb.append(", command=").append(command);
        sb.append(", createdAt=").append(createdAt);
        sb.append('}');
        return sb.toString();
    }

}
