package com.hannaf.marte.exploration.probe;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.persistence.*;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import com.hannaf.marte.exploration.Exploration;
import com.hannaf.marte.exploration.probe.command.Command;
import com.hannaf.marte.exploration.probe.command.ExecutedCommand;

@Entity
public class Probe {

    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Valid
    @Embedded
    private Position position;

    @NotNull
    @ManyToOne
    private Exploration exploration;

    @Valid
    @OneToMany(cascade = CascadeType.MERGE)
    @JoinColumn(name = "probe_id")
    private List<ExecutedCommand> executedCommands = new ArrayList<>();

    @Deprecated
    Probe() {}

    public Long getId() {
        return id;
    }

    public Probe(Position position) {
        this.position = position;
    }

    public Position getPosition() {
        return position;
    }

    public void setExploration(Exploration exploration) {
        this.exploration = exploration;
        this.exploration.add(this);
    }

    public Exploration getExploration() {
        return exploration;
    }

    public List<ExecutedCommand> getExecutedCommands() {
        return Collections.unmodifiableList(executedCommands);
    }

    public void execute(List<Command> commands) {
        commands.forEach(command -> {
            position = command.getNewPositionBasedOnActual(position);

            if (position.isBeyondLimit(exploration.getMaxCoordinateToExplore())) {
                throw new IllegalArgumentException(String.format("Probe %s position value cannot be superior to %s for exploration %s",
                        id, exploration.getMaxCoordinateToExplore(), id));
            }

            executedCommands.add(new ExecutedCommand(command));
        });
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Probe{");
        sb.append("id=").append(id);
        sb.append(", position=").append(position);
        sb.append(", exploration=").append(exploration);
        sb.append(", executedCommands=").append(executedCommands);
        sb.append('}');
        return sb.toString();
    }

}
