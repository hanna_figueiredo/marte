package com.hannaf.marte.exploration.probe;

import static java.util.stream.Collectors.toList;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.hannaf.marte.exploration.Coordinate;
import com.hannaf.marte.exploration.Exploration;
import com.hannaf.marte.exploration.ExplorationRepository;
import com.hannaf.marte.exploration.probe.command.CommandsDTO;

@RequestMapping("/explorations/{explorationId}/probes")
@RestController
public class ProbeController {

    @Autowired
    private ExplorationRepository explorationRepository;

    @Autowired
    private ProbeRepository probeRepository;

    @Autowired
    private ProbeToProbeDTOConverter converter;

    @PostMapping
    public ResponseEntity<ProbeDTO> save(@PathVariable Long explorationId, @RequestBody @Valid ProbeCreateDTO probeCreateDTO) {
        Optional<Exploration> exploration = explorationRepository.findById(explorationId);

        if (exploration.isPresent()) {
            Probe probe = new Probe(Position.createInitialBy(new Coordinate(probeCreateDTO.getInitialXCoordinate(), probeCreateDTO.getInitialYCoordinate()), probeCreateDTO.getInitialDirection()));

            probe.setExploration(exploration.get());

            probeRepository.save(probe);

            return ResponseEntity.status(HttpStatus.CREATED).body(converter.convert(probe));
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @GetMapping
    public ResponseEntity<List<ProbeDTO>> list(@PathVariable Long explorationId) {
        Optional<Exploration> exploration = explorationRepository.findById(explorationId);

        if (exploration.isPresent()) {
            List<ProbeDTO> probes = exploration.get().getProbes().stream().map(probe -> converter.convert(probe)).collect(toList());

            return ResponseEntity.ok(probes);
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @GetMapping("/{probeId}")
    public ResponseEntity<ProbeDTO> get(@PathVariable Long explorationId, @PathVariable Long probeId) {
        Optional<Probe> probe = probeRepository.findById(probeId);

        if (probe.isPresent() && probe.get().getExploration().getId().equals(explorationId)) {
            return ResponseEntity.ok(converter.convert(probe.get()));
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @PatchMapping("/{probeId}")
    public ResponseEntity<ProbeDTO> executeCommands(@PathVariable Long explorationId, @PathVariable Long probeId, @Valid @NotNull @RequestBody CommandsDTO commandsDTO) {
        Optional<Probe> probe = probeRepository.findById(probeId);

        if (probe.isPresent() && probe.get().getExploration().getId().equals(explorationId)) {
            probe.get().execute(commandsDTO.getCommands());

            probeRepository.save(probe.get());

            return ResponseEntity.ok(converter.convert(probe.get()));
        } else {
            return ResponseEntity.notFound().build();
        }
    }

}
