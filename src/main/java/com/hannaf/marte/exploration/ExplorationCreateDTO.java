package com.hannaf.marte.exploration;

import javax.validation.constraints.Min;

public class ExplorationCreateDTO {

    @Min(0)
    private int maxXCoordinate;
    @Min(0)
    private int maxYCoordinate;

    public int getMaxXCoordinate() {
        return maxXCoordinate;
    }

    public ExplorationCreateDTO setMaxXCoordinate(int maxXCoordinate) {
        this.maxXCoordinate = maxXCoordinate;
        return this;
    }

    public int getMaxYCoordinate() {
        return maxYCoordinate;
    }

    public ExplorationCreateDTO setMaxYCoordinate(int maxYCoordinate) {
        this.maxYCoordinate = maxYCoordinate;
        return this;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("ExplorationCreateDTO{");
        sb.append("maxXCoordinate=").append(maxXCoordinate);
        sb.append(", maxYCoordinate=").append(maxYCoordinate);
        sb.append('}');
        return sb.toString();
    }
}
