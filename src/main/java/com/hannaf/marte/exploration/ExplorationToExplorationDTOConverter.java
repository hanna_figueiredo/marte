package com.hannaf.marte.exploration;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.hannaf.marte.exploration.probe.ProbeDTO;
import com.hannaf.marte.exploration.probe.ProbeToProbeDTOConverter;

@Component
public class ExplorationToExplorationDTOConverter implements Converter<Exploration, ExplorationDTO> {

    @Autowired
    private ProbeToProbeDTOConverter converter;

    @Override
    public ExplorationDTO convert(Exploration exploration) {
        ExplorationDTO explorationDTO = new ExplorationDTO();
        explorationDTO.setId(exploration.getId());
        explorationDTO.setMaxXCoordinate(exploration.getMaxCoordinateToExplore().getX());
        explorationDTO.setMaxYCoordinate(exploration.getMaxCoordinateToExplore().getY());

        List<ProbeDTO> probes = exploration.getProbes().stream().map(probe -> converter.convert(probe)).collect(Collectors.toList());

        explorationDTO.setProbes(probes);

        return explorationDTO;
    }

}
