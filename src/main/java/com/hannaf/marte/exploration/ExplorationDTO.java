package com.hannaf.marte.exploration;

import java.util.List;

import com.hannaf.marte.exploration.probe.ProbeDTO;

public class ExplorationDTO {

    private Long id;
    private int maxXCoordinate;
    private int maxYCoordinate;
    private List<ProbeDTO> probes;

    public Long getId() {
        return id;
    }

    public ExplorationDTO setId(Long id) {
        this.id = id;
        return this;
    }

    public int getMaxXCoordinate() {
        return maxXCoordinate;
    }

    public ExplorationDTO setMaxXCoordinate(int maxXCoordinate) {
        this.maxXCoordinate = maxXCoordinate;
        return this;
    }

    public int getMaxYCoordinate() {
        return maxYCoordinate;
    }

    public ExplorationDTO setMaxYCoordinate(int maxYCoordinate) {
        this.maxYCoordinate = maxYCoordinate;
        return this;
    }

    public List<ProbeDTO> getProbes() {
        return probes;
    }

    public ExplorationDTO setProbes(List<ProbeDTO> probes) {
        this.probes = probes;
        return this;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("ExplorationDTO{");
        sb.append("id=").append(id);
        sb.append(", maxXCoordinate=").append(maxXCoordinate);
        sb.append(", maxYCoordinate=").append(maxYCoordinate);
        sb.append(", probes=").append(probes);
        sb.append('}');
        return sb.toString();
    }
}
