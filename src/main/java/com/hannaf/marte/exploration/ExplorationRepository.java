package com.hannaf.marte.exploration;

import java.util.List;
import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

public interface ExplorationRepository extends CrudRepository<Exploration, Long> {

    List<Exploration> findAll();

    Optional<Exploration> findById(Long explorationId);
}
