insert into exploration (id, x, y) values (1, 3, 7);
insert into exploration (id, x, y) values (2, 5, 2);

insert into probe (id, x, y, direction, exploration_id) values (1, 1, 3, 'S', 2);
insert into probe (id, x, y, direction, exploration_id) values (2, 2, 5, 'W', 2);

insert into executed_command (id, command, created_at, probe_id) values (8, 'LEFT', '2016-09-05 18:05:11.685+00', 2);
insert into executed_command (id, command, created_at, probe_id) values (9, 'MOVE', '2016-09-05 18:05:11.685+00', 2);
insert into executed_command (id, command, created_at, probe_id) values (10, 'RIGHT', '2016-09-05 18:05:11.685+00', 2);
