SET REFERENTIAL_INTEGRITY FALSE;
truncate table probe;
truncate table executed_command;
truncate table exploration;
SET REFERENTIAL_INTEGRITY TRUE;
