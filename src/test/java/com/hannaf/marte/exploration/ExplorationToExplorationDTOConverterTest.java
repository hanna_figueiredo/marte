package com.hannaf.marte.exploration;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

import java.util.Arrays;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.hannaf.marte.exploration.probe.Probe;
import com.hannaf.marte.exploration.probe.ProbeDTO;
import com.hannaf.marte.exploration.probe.ProbeToProbeDTOConverter;

@RunWith(MockitoJUnitRunner.class)
public class ExplorationToExplorationDTOConverterTest {

    @Mock
    private ProbeToProbeDTOConverter probeToProbeDTOConverter;

    @InjectMocks
    private ExplorationToExplorationDTOConverter converter;

    @Test
    public void shouldConvertExploration() {
        Exploration exploration = mock(Exploration.class, RETURNS_DEEP_STUBS);
        when(exploration.getId()).thenReturn(1l);
        when(exploration.getMaxCoordinateToExplore().getX()).thenReturn(5);
        when(exploration.getMaxCoordinateToExplore().getY()).thenReturn(67);
        Probe probe = mock(Probe.class);
        when(exploration.getProbes()).thenReturn(Arrays.asList(probe));
        ProbeDTO probeDto = mock(ProbeDTO.class);
        when(probeToProbeDTOConverter.convert(probe)).thenReturn(probeDto);

        ExplorationDTO dto = converter.convert(exploration);

        assertEquals(exploration.getId(), dto.getId());
        assertEquals(exploration.getMaxCoordinateToExplore().getX(), dto.getMaxXCoordinate());
        assertEquals(exploration.getMaxCoordinateToExplore().getY(), dto.getMaxYCoordinate());
        assertEquals(1, dto.getProbes().size());
        assertEquals(probeDto, dto.getProbes().get(0));
    }

}
