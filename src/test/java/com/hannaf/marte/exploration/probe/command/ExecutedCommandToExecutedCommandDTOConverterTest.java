package com.hannaf.marte.exploration.probe.command;

import static java.time.LocalDateTime.now;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import org.junit.Test;

public class ExecutedCommandToExecutedCommandDTOConverterTest {

    private ExecutedCommandToExecutedCommandDTOConverter converter = new ExecutedCommandToExecutedCommandDTOConverter();

    @Test
    public void shouldConvertExecutedCommand() {
        ExecutedCommand command = mock(ExecutedCommand.class);
        when(command.getCommand()).thenReturn(Command.LEFT);
        when(command.getCreatedAt()).thenReturn(now());

        ExecutedCommandDTO dto = converter.convert(command);

        assertEquals(command.getCommand(), dto.getCommand());
        assertEquals(command.getCreatedAt(), dto.getCreatedAt());
    }

}