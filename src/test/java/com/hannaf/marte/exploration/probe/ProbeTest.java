package com.hannaf.marte.exploration.probe;

import java.util.Arrays;

import org.junit.Assert;
import org.junit.Test;

import com.hannaf.marte.exploration.Coordinate;
import com.hannaf.marte.exploration.Exploration;
import com.hannaf.marte.exploration.probe.command.Command;

public class ProbeTest {

    @Test
    public void shouldExecuteCommandsMovingLeft() {
        Probe probe = new Probe(Position.createInitialBy(new Coordinate(1, 2), Direction.N));
        probe.setExploration(new Exploration(new Coordinate(5, 5)));

        probe.execute(Arrays.asList(Command.LEFT, Command.MOVE, Command.LEFT, Command.MOVE));
        probe.execute(Arrays.asList(Command.LEFT, Command.MOVE, Command.LEFT));
        probe.execute(Arrays.asList(Command.MOVE, Command.MOVE));

        Assert.assertEquals(Direction.N, probe.getPosition().getDirection());
        Assert.assertEquals(1, probe.getPosition().getCoordinate().getX());
        Assert.assertEquals(3, probe.getPosition().getCoordinate().getY());
        Assert.assertEquals(9, probe.getExecutedCommands().size());
    }

    @Test
    public void shouldExecuteCommandsMovingRight() {
        Probe probe = new Probe(Position.createInitialBy(new Coordinate(3, 3), Direction.E));
        probe.setExploration(new Exploration(new Coordinate(5, 5)));

        probe.execute(Arrays.asList(Command.MOVE, Command.MOVE, Command.RIGHT, Command.MOVE));
        probe.execute(Arrays.asList(Command.MOVE, Command.RIGHT, Command.MOVE));
        probe.execute(Arrays.asList(Command.RIGHT, Command.RIGHT, Command.MOVE));

        Assert.assertEquals(Direction.E, probe.getPosition().getDirection());
        Assert.assertEquals(5, probe.getPosition().getCoordinate().getX());
        Assert.assertEquals(1, probe.getPosition().getCoordinate().getY());
        Assert.assertEquals(10, probe.getExecutedCommands().size());
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldThrowExceptionWhenCommandsAreBeyondLimit() {
        Probe probe = new Probe(Position.createInitialBy(new Coordinate(3, 3), Direction.E));
        probe.setExploration(new Exploration(new Coordinate(5, 5)));

        probe.execute(Arrays.asList(Command.MOVE, Command.MOVE, Command.MOVE));

        Assert.assertEquals(Direction.E, probe.getPosition().getDirection());
        Assert.assertEquals(5, probe.getPosition().getCoordinate().getX());
        Assert.assertEquals(1, probe.getPosition().getCoordinate().getY());
        Assert.assertEquals(10, probe.getExecutedCommands().size());
    }

}
