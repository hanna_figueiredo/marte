package com.hannaf.marte.exploration.probe;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

import java.util.Arrays;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.hannaf.marte.exploration.probe.command.ExecutedCommand;
import com.hannaf.marte.exploration.probe.command.ExecutedCommandDTO;
import com.hannaf.marte.exploration.probe.command.ExecutedCommandToExecutedCommandDTOConverter;

@RunWith(MockitoJUnitRunner.class)
public class ProbeToProbeDTOConverterTest {

    @Mock
    private ExecutedCommandToExecutedCommandDTOConverter executedCommandToExecutedCommandDTOConverter;

    @InjectMocks
    private ProbeToProbeDTOConverter convert;

    @Test
    public void shouldConvertProbe() {
        Probe probe = mock(Probe.class, RETURNS_DEEP_STUBS);
        when(probe.getId()).thenReturn(1l);
        when(probe.getPosition().getCoordinate().getY()).thenReturn(45);
        when(probe.getPosition().getCoordinate().getX()).thenReturn(32);
        when(probe.getPosition().getDirection()).thenReturn(Direction.W);
        ExecutedCommand command = mock(ExecutedCommand.class);
        when(probe.getExecutedCommands()).thenReturn(Arrays.asList(command));
        ExecutedCommandDTO commandDTO = mock(ExecutedCommandDTO.class);
        when(executedCommandToExecutedCommandDTOConverter.convert(command)).thenReturn(commandDTO);

        ProbeDTO dto = this.convert.convert(probe);

        assertEquals(probe.getId(), dto.getId());
        assertEquals(probe.getPosition().getCoordinate().getX(), dto.getCoordinateX());
        assertEquals(probe.getPosition().getCoordinate().getY(), dto.getCoordinateY());
        assertEquals(probe.getPosition().getDirection(), dto.getDirection());
        assertEquals(1, dto.getExecutedCommands().size());
        assertEquals(commandDTO, dto.getExecutedCommands().get(0));
    }

}