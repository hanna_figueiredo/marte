package com.hannaf.marte.exploration.probe;

import org.junit.Assert;
import org.junit.Test;

public class DirectionTest {

    @Test
    public void shouldReturnRightDirectionsFromNorth() {
        Assert.assertEquals(Direction.W, Direction.N.getLeft());
        Assert.assertEquals(Direction.E, Direction.N.getRight());
    }

    @Test
    public void shouldReturnRightDirectionsFromEast() {
        Assert.assertEquals(Direction.N, Direction.E.getLeft());
        Assert.assertEquals(Direction.S, Direction.E.getRight());
    }

    @Test
    public void shouldReturnRightDirectionsFromSouth() {
        Assert.assertEquals(Direction.E, Direction.S.getLeft());
        Assert.assertEquals(Direction.W, Direction.S.getRight());
    }

    @Test
    public void shouldReturnRightDirectionsFromWest() {
        Assert.assertEquals(Direction.S, Direction.W.getLeft());
        Assert.assertEquals(Direction.N, Direction.W.getRight());
    }
}