package com.hannaf.marte.exploration.probe;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.MOCK)
@AutoConfigureMockMvc
@RunWith(SpringRunner.class)
@Sql(scripts = "classpath:test_population.sql", executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
@Sql(scripts = "classpath:clean_population.sql", executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
public class ProbeControllerTest {

    private static final Long EXPLORATION_ID_1 = 1l;
    private static final Long EXPLORATION_ID_2 = 2l;
    private static final Long EXPLORATION_NONEXISTENT_ID = 42l;
    private static final Long PROBE_ID_1 = 1l;
    private static final Long PROBE_NONEXISTENT_ID = 42l;

    @Autowired
    private MockMvc mvc;

    @Test
    public void shouldCreateNewProbeForExploration() throws Exception {
        mvc.perform(post("/explorations/" + EXPLORATION_ID_1 + "/probes").contentType(MediaType.APPLICATION_JSON).content("{\"initialXCoordinate\":3,\"initialYCoordinate\":6,\"initialDirection\":\"W\"}"))
                .andExpect(status().isCreated())
                .andExpect(MockMvcResultMatchers.content().json("{\"id\":3,\"coordinateX\":3,\"coordinateY\":6,\"direction\":\"W\",\"executedCommands\":[]}"));
    }

    @Test
    public void shouldReturnNotFountWheTryingToAddProbeToNonexistentExploration() throws Exception {
        mvc.perform(post("/explorations/" + EXPLORATION_NONEXISTENT_ID + "/probes").contentType(MediaType.APPLICATION_JSON).content("{\"initialXCoordinate\":3,\"initialYCoordinate\":6,\"initialDirection\":\"W\"}"))
                .andExpect(status().isNotFound());
    }

    @Test
    public void shouldGetProbesFromExploration() throws Exception {
        mvc.perform(get("/explorations/" + EXPLORATION_ID_2 + "/probes"))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.content().json("[{\"id\":1,\"coordinateX\":1,\"coordinateY\":3,\"direction\":\"S\",\"executedCommands\":[]}" +
                        ",{\"id\":2,\"coordinateX\":2,\"coordinateY\":5,\"direction\":\"W\"" +
                        ",\"executedCommands\":[{\"command\":\"LEFT\",\"createdAt\":\"2016-09-05T15:05:11.685\"}" +
                        ",{\"command\":\"MOVE\",\"createdAt\":\"2016-09-05T15:05:11.685\"},{\"command\":\"RIGHT\"" +
                        ",\"createdAt\":\"2016-09-05T15:05:11.685\"}]}]"));
    }

    @Test
    public void shouldReturnNotFoundWhenTryingToGetProbesFromNonexistentExploration() throws Exception {
        mvc.perform(get("/explorations/" + EXPLORATION_NONEXISTENT_ID + "/probes"))
                .andExpect(status().isNotFound());
    }

    @Test
    public void shouldGetProbeForExploration() throws Exception {
        mvc.perform(get("/explorations/" + EXPLORATION_ID_2 + "/probes/" + PROBE_ID_1))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.content().json("{\"id\":1,\"coordinateX\":1,\"coordinateY\":3,\"direction\":\"S\",\"executedCommands\":[]}"));
    }

    @Test
    public void shouldReturnNotFoundForNonexistentProbe() throws Exception {
        mvc.perform(get("/explorations/" + EXPLORATION_ID_1 + "/probes/" + PROBE_NONEXISTENT_ID))
                .andExpect(status().isNotFound());
    }

    @Test
    public void shouldReturnNotFoundProbeDoesNotBelongToExpedition() throws Exception {
        mvc.perform(get("/explorations/" + EXPLORATION_ID_1 + "/probes/" + PROBE_ID_1))
                .andExpect(status().isNotFound());
    }

    @Test
    public void shouldSendCommandsToProbeForExploration() throws Exception {
        mvc.perform(patch("/explorations/" + EXPLORATION_ID_2 + "/probes/" + PROBE_ID_1)
                .contentType(MediaType.APPLICATION_JSON).content("{\"commands\":[\"MOVE\",\"LEFT\",\"MOVE\",\"RIGHT\"]}"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id", is(1)))
                .andExpect(jsonPath("$.coordinateX", is(2)))
                .andExpect(jsonPath("$.coordinateY", is(2)))
                .andExpect(jsonPath("$.executedCommands", hasSize(4)))
                .andExpect(jsonPath("$.executedCommands[0].command", is("MOVE")))
                .andExpect(jsonPath("$.executedCommands[1].command", is("LEFT")))
                .andExpect(jsonPath("$.executedCommands[2].command", is("MOVE")))
                .andExpect(jsonPath("$.executedCommands[3].command", is("RIGHT")));
    }
}
