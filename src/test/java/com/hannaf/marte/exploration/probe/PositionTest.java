package com.hannaf.marte.exploration.probe;

import static com.hannaf.marte.exploration.probe.Position.createInitialBy;
import static org.junit.Assert.assertEquals;

import org.junit.Test;

import com.hannaf.marte.exploration.Coordinate;

public class PositionTest {

    @Test
    public void shouldReturnFalseWhenCoordinateXIsBiggerThanLimitCoordinateX() {
        Position position = createInitialBy(new Coordinate(3, 3), Direction.E);

        position.isBeyondLimit(new Coordinate(2, 3));
    }

    @Test
    public void shouldReturnFalseWhenCoordinateYIsBiggerThanLimitCoordinateY() {
        Position position = createInitialBy(new Coordinate(3, 3), Direction.E);

        position.isBeyondLimit(new Coordinate(3, 2));
    }

    @Test
    public void shouldReturnNewPositionWhenTurnLeft() {
        Position position = Position.turnLeftFrom(createInitialBy(new Coordinate(3, 3), Direction.E));

        assertEquals(3, position.getCoordinate().getX());
        assertEquals(3 ,position.getCoordinate().getY());
        assertEquals(Direction.N, position.getDirection());
    }

    @Test
    public void shouldReturnNewPositionWhenTurnRight() {
        Position position = Position.turnRightFrom(createInitialBy(new Coordinate(3, 3), Direction.E));

        assertEquals(3, position.getCoordinate().getX());
        assertEquals(3 ,position.getCoordinate().getY());
        assertEquals(Direction.S, position.getDirection());
    }

    @Test
    public void shouldReturnNewPositionWhenMoveForward() {
        Position position = Position.moveForwardFrom(createInitialBy(new Coordinate(3, 3), Direction.E));

        assertEquals(4, position.getCoordinate().getX());
        assertEquals(3 ,position.getCoordinate().getY());
        assertEquals(Direction.E, position.getDirection());
    }

}