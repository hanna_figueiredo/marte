package com.hannaf.marte.exploration;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import com.hannaf.marte.exploration.probe.Direction;
import com.hannaf.marte.exploration.probe.Position;
import com.hannaf.marte.exploration.probe.Probe;

public class ExplorationTest {

    @Test
    public void shouldAddNewProbeToExploration() {
        Exploration exploration = new Exploration(new Coordinate(4, 5));
        Coordinate coordinate = new Coordinate(1, 2);
        exploration.add(new Probe(Position.createInitialBy(coordinate, Direction.E)));

        assertEquals(1, exploration.getProbes().size());
        assertEquals(coordinate, exploration.getProbes().get(0).getPosition().getCoordinate());
        assertEquals(Direction.E, exploration.getProbes().get(0).getPosition().getDirection());
    }

    @Test
    public void shouldAddNewProbeOnLimitToExploration() {
        Exploration exploration = new Exploration(new Coordinate(4, 5));
        Coordinate coordinate = new Coordinate(4, 5);
        exploration.add(new Probe(Position.createInitialBy(coordinate, Direction.E)));

        assertEquals(1, exploration.getProbes().size());
        assertEquals(coordinate, exploration.getProbes().get(0).getPosition().getCoordinate());
        assertEquals(Direction.E, exploration.getProbes().get(0).getPosition().getDirection());
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldThrowExceptionOnAddNewProbeWithCoordinateXBeyondLimitToExploration() {
        Exploration exploration = new Exploration(new Coordinate(4, 5));
        Coordinate coordinate = new Coordinate(5, 3);
        exploration.add(new Probe(Position.createInitialBy(coordinate, Direction.E)));
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldThrowExceptionOnAddNewProbeWithCoordinateYBeyondLimitToExploration() {
        Exploration exploration = new Exploration(new Coordinate(4, 5));
        Coordinate coordinate = new Coordinate(2, 6);
        exploration.add(new Probe(Position.createInitialBy(coordinate, Direction.E)));
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldThrowExceptionOnAddNewProbeWithCoordinateXAndYBeyondExplorationLimit() {
        Exploration exploration = new Exploration(new Coordinate(4, 5));
        Coordinate coordinate = new Coordinate(5, 6);
        exploration.add(new Probe(Position.createInitialBy(coordinate, Direction.E)));
    }
}