package com.hannaf.marte.exploration;

import org.junit.Assert;
import org.junit.Test;

import com.hannaf.marte.exploration.probe.Direction;

public class CoordinateTest {

    @Test
    public void shouldMoveUpYCoordinateWhenMovingNorthDirection() {
        Coordinate coordinate = new Coordinate(4, 4);

        Coordinate newCoordinate = coordinate.moveBy(Direction.N);

        Assert.assertEquals(4, newCoordinate.getX());
        Assert.assertEquals(5, newCoordinate.getY());
    }

    @Test
    public void shouldMoveDownYCoordinateWhenMovingSouthDirection() {
        Coordinate coordinate = new Coordinate(4, 4);

        Coordinate newCoordinate = coordinate.moveBy(Direction.S);

        Assert.assertEquals(4, newCoordinate.getX());
        Assert.assertEquals(3, newCoordinate.getY());
    }

    @Test
    public void shouldMoveUpXCoordinateWhenMovingEastDirection() {
        Coordinate coordinate = new Coordinate(4, 4);

        Coordinate newCoordinate = coordinate.moveBy(Direction.E);

        Assert.assertEquals(5, newCoordinate.getX());
        Assert.assertEquals(4, newCoordinate.getY());
    }

    @Test
    public void shouldMoveDownXCoordinateWhenMovingWestDirection() {
        Coordinate coordinate = new Coordinate(4, 4);

        Coordinate newCoordinate = coordinate.moveBy(Direction.W);

        Assert.assertEquals(3, newCoordinate.getX());
        Assert.assertEquals(4, newCoordinate.getY());
    }
}
