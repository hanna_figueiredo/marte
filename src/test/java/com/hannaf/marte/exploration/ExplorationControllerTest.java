package com.hannaf.marte.exploration;

import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.MOCK)
@AutoConfigureMockMvc
@RunWith(SpringRunner.class)
@Sql(scripts = "classpath:test_population.sql", executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
@Sql(scripts = "classpath:clean_population.sql", executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
public class ExplorationControllerTest {

    private static final Long EXPLORATION_ID = 1l;
    private static final Long EXPLORATION_NONEXISTENT_ID = 42l;

    @Autowired
    private MockMvc mvc;

    @Test
    public void shouldCreateNewExploration() throws Exception {
        mvc.perform(post("/explorations").contentType(APPLICATION_JSON).content("{\"maxXCoordinate\":12,\"maxYCoordinate\":7}"))
                .andExpect(status().isCreated())
                .andExpect(MockMvcResultMatchers.content().json("{\"id\":3,\"maxXCoordinate\":12,\"maxYCoordinate\":7,\"probes\":[]}"));
    }

    @Test
    public void shouldGetExplorations() throws Exception {
        mvc.perform(get("/explorations"))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.content().json("[{\"id\":1,\"maxXCoordinate\":3,\"maxYCoordinate\":7,\"probes\":[]}," +
                        "{\"id\":2,\"maxXCoordinate\":5,\"maxYCoordinate\":2" +
                        ",\"probes\":[{\"id\":1,\"coordinateX\":1,\"coordinateY\":3,\"direction\":\"S\"" +
                        ",\"executedCommands\":[]},{\"id\":2,\"coordinateX\":2,\"coordinateY\":5,\"direction\":\"W\"" +
                        ",\"executedCommands\":[{\"command\":\"LEFT\",\"createdAt\":\"2016-09-05T15:05:11.685\"}" +
                        ",{\"command\":\"MOVE\",\"createdAt\":\"2016-09-05T15:05:11.685\"}" +
                        ",{\"command\":\"RIGHT\",\"createdAt\":\"2016-09-05T15:05:11.685\"}]}]}]"));
    }

    @Test
    public void shouldGetExploration() throws Exception {
        mvc.perform(get("/explorations/" + EXPLORATION_ID))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.content().json("{\"id\":1,\"maxXCoordinate\":3,\"maxYCoordinate\":7,\"probes\":[]}"));
    }

    @Test
    public void shouldReturnNotFoundWhenExplorationNotExists() throws Exception {
        mvc.perform(get("/explorations/" + EXPLORATION_NONEXISTENT_ID))
                .andExpect(status().isNotFound());
    }
}
