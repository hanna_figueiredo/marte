# marte

Para rodar todos os testes:
```bash
mvn clean test
```

Subir aplicação (`http://localhost:8080`)
```bash
mvn clean spring-boot:run
```

Adicionar exploração:
```bash
curl -v -X POST -H "Content-e: application/json" --data '{"maxXCoordinate":12,"maxYCoordinate":7}' http://localhost:8080/explorations
```

Listar explorações:
```bash
curl -v -X GET http://localhost:8080/explorations
```

Buscar uma exploração:
```bash
curl -v -X GET http://localhost:8080/explorations/1
```

Adicionar sonda:
```bash
curl -v -X POST -H "Content-Type: application/json" --data '{"initialXCoordinate":3,"initialYCoordinate":6,"initialDirection":"W"}' http://localhost:8080/explorations/1/probes
```

Buscar lista de sondas por exploração:
```bash
curl -v -X GET http://localhost:8080/explorations/1/probes
```

Buscar sonda por exploração:
```bash
curl -v -X GET http://localhost:8080/explorations/1/probes/1
```

Enviar comandos para uma sonda:
```bash
curl -v -X PATCH -H "Content-Type: application/json" --data '{"commands":["MOVE","LEFT","MOVE","RIGHT"]}' http://localhost:8080/explorations/1/probes/1
```
